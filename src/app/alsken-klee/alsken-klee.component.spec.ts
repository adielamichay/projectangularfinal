import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlskenKleeComponent } from './alsken-klee.component';

describe('AlskenKleeComponent', () => {
  let component: AlskenKleeComponent;
  let fixture: ComponentFixture<AlskenKleeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlskenKleeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlskenKleeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
