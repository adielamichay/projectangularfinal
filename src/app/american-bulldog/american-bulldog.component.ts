import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-american-bulldog',
  templateUrl: './american-bulldog.component.html',
  styleUrls: ['./american-bulldog.component.css']
})
export class AmericanBulldogComponent implements OnInit {

  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  
  ngOnInit() {
  }

}
