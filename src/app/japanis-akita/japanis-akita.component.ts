import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-japanis-akita',
  templateUrl: './japanis-akita.component.html',
  styleUrls: ['./japanis-akita.component.css']
})
export class JapanisAkitaComponent implements OnInit {

  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  
  ngOnInit() {
  }

}
