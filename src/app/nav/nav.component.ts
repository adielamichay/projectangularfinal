import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  toVideo(){
    this.router.navigate(['/welcome']);
  }
  toGroup(){
    this.router.navigate(['/welcome']);
  }
  toMain(){
    this.router.navigate(['/welcome']);
  }
  toChat(){
    this.router.navigate(['/welcome']);
  }
  toRegister(){
    this.router.navigate(['/welcome']);
  }
  toWelcome(){
    this.router.navigate(['/welcome']);
  }
  toQuiz(){
    this.router.navigate(['/quiz']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  logout(){
    this.authService.logout().
      then(value => {
        this.router.navigate(['/login'])
          }).catch(err=>{console.log(err)})
  }
  constructor(private router:Router,
              public authService:AuthService
              ) { }

  ngOnInit() {
  }

}
