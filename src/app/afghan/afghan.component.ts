import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-afghan',
  templateUrl: './afghan.component.html',
  styleUrls: ['./afghan.component.css']
})
export class AfghanComponent implements OnInit {
  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  

  ngOnInit() {
  }

}
