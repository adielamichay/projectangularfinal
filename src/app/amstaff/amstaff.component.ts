import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-amstaff',
  templateUrl: './amstaff.component.html',
  styleUrls: ['./amstaff.component.css']
})
export class AmstaffComponent implements OnInit {

  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  
  ngOnInit() {
  }

}
