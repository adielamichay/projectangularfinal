import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-american-eskimos',
  templateUrl: './american-eskimos.component.html',
  styleUrls: ['./american-eskimos.component.css']
})
export class AmericanEskimosComponent implements OnInit {

  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  
 

  ngOnInit() {
  }

}
