import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})


export class WelcomeComponent implements OnInit {

 
  alaskanMalamute(){
    this.router.navigate(['/alskenMelmote']);
  }
  
  alskenKlee(){
    this.router.navigate(['/alskenKlee']);
  }
  amstaff(){
    this.router.navigate(['/amstaff']);
  }
  americanBulldog(){
    this.router.navigate(['/americanBulldog']);
  }
  americanPit(){
    this.router.navigate(['/americanPit']);
  }
  americanEskimos(){
    this.router.navigate(['/americanEskimos']);
  }
  afghan(){
    this.router.navigate(['/afghan']);
  }
  affenpinschers(){
    this.router.navigate(['/affenpinschers']);
  }
  japanisAkita(){
    this.router.navigate(['/japanisAkita']);
  }
   
  constructor(private router:Router) { }

  ngOnInit() {
  }
  

}


